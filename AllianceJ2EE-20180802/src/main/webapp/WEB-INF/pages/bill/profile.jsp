<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<head>
	<c:url value="/css/bootstrap/bootstrap.min.css" var="main_css"></c:url>
	<link href="${main_css}" rel="stylesheet" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="/js/bootstrapjs/bootstrap.js"></script>
	
</head>
<body>
 


<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
  <ul class="navbar-nav">
    <li class="nav-item active">	
      <a class="nav-link" href="#">Profile</a>
    </li>
  </ul>
</nav>
<div class="container" style="margin-top: 20px;">	
	<div id="list">
		<form method="POST" action="http://localhost:8010/bill/save">
			<h3 class="display-4">Hello!</h3>	
			<p class="lead">List of students | Check to delete users </p>
			<c:forEach items="${studentList}" var="entry">			
				<div class = "table-responsive">
					<table class="table">
							<tr>
							<td>
							<div class="form-check">
							    <input type="checkbox" name="userId" class="form-check-input" id="userId" value="${entry.id}">
							    <label id ="sid" class="form-check-label font-weight-bold" for="userId" >ID: ${entry.id}</label>
							    <p class="lead">Name: ${entry.lastName}, ${entry.firstName}</p>					    
							    <p>Username: ${entry.username} | Type: ${entry.type} </p>
						  	</div>
		  					</td>
						</tr>
					</table>
				</div>
			</c:forEach>
			<br/><br/>		
			<button class="btn btn-success" type="submit">Delete</button>					
  			<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Add/Update</button>  			
		</form>
		<form method="POST" action="http://localhost:8010/bill/logout">		
			<button type="submit" class="float-right btn btn-danger">Log out</button>
		</form>
	</div>
	</div>	




  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title float-left">Add/Update User</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        	<form method="POST" action="http://localhost:8010/bill/save">				
			<div class="form-group">
				    <label for="studentId">Student ID</label>
				    <input type="text" class="form-control" name="studentId" id="studentId" placeholder="">
			</div>		
			<div class="form-group">
				    <label for="username">Username</label>
				    <input type="text" class="form-control" name="username" id="username" placeholder="">
			</div>
			<div class="form-group">
				    <label for="password">First name</label>
				    <input type="text" class="form-control" name="firstName" id="firstName" placeholder="">
			</div>
			<div class="form-group">
				    <label for="password">Last name</label>
				    <input type="text" class="form-control" name="lastName" id="lastName" placeholder="">
			</div>
			<div class="form-group">
				    <label for="password">Password</label>
				    <input type="password" class="form-control" name="password" id="password" placeholder="">
			</div>
			<div class="form-group">
				    <label for="password">Type</label>
				    <input type="text" class="form-control" name="type" id="type" placeholder="">
			</div>
			<button class="btn btn-success" type="submit">Save</button>
		</form>
		
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  



</body>
</html>
