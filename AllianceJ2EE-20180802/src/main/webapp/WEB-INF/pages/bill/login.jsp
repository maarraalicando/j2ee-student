<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<head>
	<c:url value="/css/bootstrap/bootstrap.min.css" var="main_css"></c:url>
	<link href="${main_css}" rel="stylesheet" />	
</head>
<body>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
  <ul class="navbar-nav">
    <li class="nav-item active">
      <a class="nav-link" href="#">J2EE</a>
    </li>
  </ul>
</nav>

<div class="container" style="margin-top: 50px">
	<div class="jumbotron">
	<h1 class="display-4">Hello, User!</h1>
	<p class="lead">${message}</p>
	<hr class="my-4">
	<div class="form">		
		<form method="POST" action="http://localhost:8010/bill/login">		
			<div class="form-group">
			    <label for="username">Username</label>
			    <input type="text" class="form-control" name="username" id="username" placeholder="">
		  	</div>
		  	<div class="form-group">
			    <label for="password">Password</label>
			    <input type="password" class="form-control" name="password" id="password" placeholder="">
		  	</div>
			<button type="submit" class="btn btn-success">Login</button>
			<button class = "btn btn-danger float-right"><a href="/bill/register" style="text-decoration: none; color: white;">Register</a></button>
		</form>
		
	</div>
</div>





</body>
<script src="/js/bootstrapjs/bootstrap.js"></script>
</html>
