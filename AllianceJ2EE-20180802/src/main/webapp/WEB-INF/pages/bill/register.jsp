<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en">
<head>
	<c:url value="/css/bootstrap/bootstrap.min.css" var="main_css"></c:url>
	<link href="${main_css}" rel="stylesheet" />	
</head>
<body>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
  <ul class="navbar-nav">
    <li class="nav-item active">
      <a class="nav-link" href="#">Register</a>
    </li>
  </ul>
</nav>

<div class="container" style="margin-top: 50px">
	<div class="jumbotron">
	<div class="form">		
		<form method="POST" action="http://localhost:8010/bill/created">
			<h3 class="display-4">Register</h3>			
			<hr class="my-4">
			<div class="form-group">
				    <label for="username">Username</label>
				    <input type="text" class="form-control" name="username" id="username" placeholder="">
			</div>
			<div class="form-group">
				    <label for="firstName">First name</label>
				    <input type="text" class="form-control" name="firstName" id="firstName" placeholder="">
			</div>
			<div class="form-group">
				    <label for="lastName">Last name</label>
				    <input type="text" class="form-control" name="lastName" id="lastName" placeholder="">
			</div>
			<div class="form-group">
				    <label for="password">Password</label>
				    <input type="password" class="form-control" name="password" id="password" placeholder="">
			</div>
			<div class="form-group">
				    <label for="type">Type</label>
				    <input type="text" class="form-control" name="type" id="type" placeholder="">
			</div>
		<button class="btn btn-success" type="submit">Register</button>
	</form>
	
		<button class = "btn btn-danger float-right"><a href="/bill/login" style="text-decoration: none; color: white;">Cancel</a></button>
	</div>
</div>



</body>
<script src="/js/bootstrapjs/bootstrap.js"></script>
</html>