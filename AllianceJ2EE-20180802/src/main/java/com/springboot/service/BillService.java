package com.springboot.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.entities.Student;
import com.springboot.repository.custom.BillRepository;

@Service
public class BillService {

	@Autowired
	private BillRepository billRepository;

	public boolean validateIfUserExists(String username, String password) {
		// boolean exists = false;
		// if(username != null && username.length() > 5) {
		// exists = true;
		// }
		boolean exists = billRepository.userExists(username, password);
		return exists;
	}

	public List<Student> getStudentList() {
		return billRepository.getStudents();
	}
	
	public String getStudentId(String username, String password) {
		return billRepository.getStudentId(username, password);
	}
	
//	public StudentDetail getStudentDetails(String studentId) {
//		return billRepository.getStudentDetail(studentId);
//	}
	public void addStudent(Student studentToSave) {
		billRepository.addStudent(studentToSave);

	}

	public void updateStudent(Student studentToSave) {
		// Retrieve the student to be updated
		Student student = billRepository.getStudentById(Integer.toString(studentToSave.getId()));
		if (null == student) {
			student = new Student();
		}
		student.setFirstName(studentToSave.getFirstName());
		student.setLastName(studentToSave.getLastName());
		student.setType(studentToSave.getType());
		student.setPassword(studentToSave.getPassword());
		student.setUsername(studentToSave.getUsername());
		billRepository.updateStudent(student);
		// Save the update
	}

	public void deleteStudents(String[] userIdsToDelete) {
		billRepository.deleteStudents(userIdsToDelete);
	}
}
