package com.springboot.repository.custom;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.springboot.entities.Student;

@Repository
@Transactional
public class BillRepository {

	@PersistenceContext
	private EntityManager em;

	public boolean userExists(String username, String password) {
		boolean exists = false;

		Session session = em.unwrap(Session.class);
		StringBuilder stringQuery = new StringBuilder(
				"SELECT * FROM student WHERE username = :username AND password = :password");
		SQLQuery query = session.createSQLQuery(stringQuery.toString());
		query.setParameter("username", username);
		query.setParameter("password", password);
		List list = query.list();
		// exists = (list != null && list.size() > 0);

		if (list != null && list.size() > 0) {
			exists = true;
		}

		return exists;
	}
	
	public String getStudentId(String username, String password) {
//		boolean exists = false;

		Session session = em.unwrap(Session.class);
		StringBuilder stringQuery = new StringBuilder(
				"SELECT studentId FROM student WHERE username = :username AND password = :password");
		SQLQuery query = session.createSQLQuery(stringQuery.toString());
		query.setParameter("username", username);
		query.setParameter("password", password);
		String id = Integer.toString(query.getFirstResult());
//		List list = query.list();
//		// exists = (list != null && list.size() > 0);
//
//		if (list != null && list.size() > 0) {
//			exists = true;
//		}

		return id;
	}

	public List<Student> getStudents() {
		List<Student> studentList = new ArrayList<Student>();
		Query studentQuery = em.createQuery("From Student");

		studentList = studentQuery.getResultList();

		return studentList;
	}

	public void addStudent(Student studentToSave) {
		em.persist(studentToSave);
	}

	public Student getStudentById(String studentId) {
		Student student = null;
		try {
			Query studentQuery = em.createQuery("From Student s where s.id = :studentId");
			studentQuery.setParameter("studentId", Integer.parseInt(studentId));

			student = (Student) studentQuery.setMaxResults(1).getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return student;
	}
	
//	public StudentDetail getStudentDetail(String studentId) {
//		StudentDetail student_detail = null;
//		try {
//			Query studentQuery = em.createQuery("From student_details s where s.id = :studentId");
//			studentQuery.setParameter("studentId", Integer.parseInt(studentId));
//
//			student_detail = (StudentDetail) studentQuery.setMaxResults(1).getSingleResult();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		return student_detail;
//	}
	
	public void updateStudent(Student student) {
		em.merge(student);
	}

	public void deleteStudents(String[] userIdsToDelete) {
			List<Integer> userIds = new ArrayList<Integer>();
			for(String userIdToDelete : userIdsToDelete) {
				userIds.add(Integer.parseInt(userIdToDelete));
			}
			Query studentQuery = em.createQuery("DELETE From Student s where s.id IN (:studentIdList)");
			studentQuery.setParameter("studentIdList", userIds);
			studentQuery.executeUpdate();
		
	}
}
