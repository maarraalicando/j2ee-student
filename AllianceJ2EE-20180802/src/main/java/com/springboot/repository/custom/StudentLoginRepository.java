package com.springboot.repository.custom;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;
import org.springframework.ui.ModelMap;
@Repository
@Transactional
public class StudentLoginRepository {
	@PersistenceContext
	private EntityManager em;
	
	public void addStudent() {
		
	}
	public void updateStudent() {
		
	}
	public void deleteStudent() {
		
	}
	public void getStudents() {
		
	}
	
	public void loginStudent() {
	}
	
	
}
