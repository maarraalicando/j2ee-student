package com.springboot.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.springboot.entities.Student;
import com.springboot.service.BillService;

@Controller
@RequestMapping("/bill")
public class BillController {

	@Autowired
	private BillService billService;

	@RequestMapping("/login")
	public String login(ModelMap map, HttpServletRequest request) {
		if (null != request.getSession() && null != request.getSession().getAttribute("isLoggedIn"))
			return "bill/profile";

		String username = request.getParameter("username");
		String password = request.getParameter("password");
		boolean implementCheck = (null != username && null != password);
		if (implementCheck) {
			boolean userExists = billService.validateIfUserExists(username, password);
			if (userExists) {
				request.getSession().setAttribute("isLoggedIn", true);
				List<Student> studentList = billService.getStudentList();
				System.out.println(studentList.get(0));
				map.addAttribute("studentList", studentList);
				return "bill/profile";
			} else {
				map.addAttribute("message", "Sorry you are not logged in.");
			}
		}
		return "bill/login";
	}
	@RequestMapping("/logout")
	public String logout(ModelMap map, HttpServletRequest request) {
		request.getSession().invalidate();
		return "bill/login";
	}



	@RequestMapping("/hi")
	public String hiBill(HttpServletRequest request, ModelMap map) {
		if (null == request.getSession() || null == request.getSession().getAttribute("isLoggedIn"))
			return login(map, request);
		List<Student> studentList = billService.getStudentList();
		map.addAttribute("sList", studentList);

		return "bill/hi";
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveBill(ModelMap map, HttpServletRequest request) {
		if (null == request.getSession() || null == request.getSession().getAttribute("isLoggedIn"))
			return login(map, request);
		String[] userIdsToDelete = request.getParameterValues("userId");
		if (null != userIdsToDelete && userIdsToDelete.length > 0) {
			billService.deleteStudents(userIdsToDelete);
		} else {

			String studentId = request.getParameter("studentId");
			String firstName = request.getParameter("firstName");
			String lastName = request.getParameter("lastName");
			String type = request.getParameter("type");
			String username = request.getParameter("username");
			String password = request.getParameter("password");

			// validations
			
			
			Student studentToSave = new Student();
			if (null != studentId && studentId.trim().length() > 0) {
				studentToSave.setId(Integer.parseInt(studentId));
			}
			studentToSave.setFirstName(firstName);
			studentToSave.setLastName(lastName);
			studentToSave.setType(type);
			studentToSave.setUsername(username);
			studentToSave.setPassword(password);
			if (null != studentId && studentId.length() > 0) {
				billService.updateStudent(studentToSave);
			} else {
				billService.addStudent(studentToSave);
			}
		}
		List<Student> studentList = billService.getStudentList();
		//System.out.println(studentList.get(0));
		map.addAttribute("studentList", studentList);
		
		return "bill/profile";
	}
	
	@RequestMapping(value = "/details", method = RequestMethod.POST)
	public String saveDetails(ModelMap map, HttpServletRequest request) {
//		if (null == request.getSession() || null == request.getSession().getAttribute("isLoggedIn"))
//			return login(map, request);
		String[] userIdsToDelete = request.getParameterValues("userId");
		if (null != userIdsToDelete && userIdsToDelete.length > 0) {
			billService.deleteStudents(userIdsToDelete);
		} else {

			String studentId = request.getParameter("studentId");
			String firstName = request.getParameter("firstName");
			String lastName = request.getParameter("lastName");
			String type = request.getParameter("type");
			String username = request.getParameter("username");
			String password = request.getParameter("password");

			// validations

			Student studentToSave = new Student();
			if (null != studentId && studentId.trim().length() > 0) {
				studentToSave.setId(Integer.parseInt(studentId));
			}
			studentToSave.setFirstName(firstName);
			studentToSave.setLastName(lastName);
			studentToSave.setType(type);
			studentToSave.setUsername(username);
			studentToSave.setPassword(password);
			if (null != studentId && studentId.length() > 0) {
				billService.updateStudent(studentToSave);
			}
		}
		return hiBill(request, map);
	}
	@RequestMapping(value = "/created", method = RequestMethod.POST)
	public String registerUser(ModelMap map, HttpServletRequest request) {
			String firstName = request.getParameter("firstName");
			String lastName = request.getParameter("lastName");
			String type = request.getParameter("type");
			String username = request.getParameter("username");
			String password = request.getParameter("password");

			// validations
			Student studentToSave = new Student();
			studentToSave.setFirstName(firstName);
			studentToSave.setLastName(lastName);
			studentToSave.setType(type);
			studentToSave.setUsername(username);
			studentToSave.setPassword(password);
			billService.addStudent(studentToSave);
	
		return "bill/login";

	}
	@RequestMapping("/register")
	public String redirectToRegister() {
		return "bill/register";

	}
//	@RequestMapping("/student_creation") // student_creation.jsp
//	public String student_creation(ModelMap map, HttpServletRequest request)
//	{
//	
//		return "bill/student_creation";
//	}
	

	@RequestMapping("/hi2")
	public String hiBill2(HttpServletRequest request, ModelMap map) {
		if (null == request.getSession() || null == request.getSession().getAttribute("isLoggedIn"))
			return login(map, request);
		String nameField = request.getParameter("nameField");
		String passField = request.getParameter("passField");
		boolean userExists = billService.validateIfUserExists(nameField, passField);

		if (!userExists) {
			map.addAttribute("message", "USER " + nameField + " DOES NOT EXIST");
		} else {
			map.addAttribute("message", "USER " + nameField + " DOES EXIST");
		}

		System.out.println("NAME FIELD " + nameField);
		System.out.println("PASS FIELD " + passField);
		map.addAttribute("userNameField", nameField);

		return "bill/2ndjsp";
	}
}