package com.springboot.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.springboot.service.StudentService;

@Controller
@RequestMapping("/activity")
public class StudentLoginController {
	@Autowired
	private StudentService studentService;
	
	@RequestMapping(value="/add",method=RequestMethod.POST)
	public String addStudent(HttpServletRequest request, ModelMap map) {
		return "";
	}
	
	@RequestMapping(value="/update", method=RequestMethod.GET)
	public String updateStudent(HttpServletRequest request, ModelMap map) {
		return "";
	}
	
	@RequestMapping(value="/read", method=RequestMethod.POST)
	public String getStudents(HttpServletRequest request, ModelMap map) {
		return "";
	}
	
	@RequestMapping(value="/delete",method=RequestMethod.POST)
	public String deleteStudent(HttpServletRequest request, ModelMap map) {
		return "";
	}
	
	@RequestMapping(value="/lgin",method=RequestMethod.POST)
	public String loginStudent(HttpServletRequest request, ModelMap map) {
		return "";
	}
}
