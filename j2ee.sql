/*
SQLyog Community v11.31 Beta1 (64 bit)
MySQL - 5.7.21-log : Database - j2ee
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`j2ee` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `j2ee`;

/*Table structure for table `student` */

DROP TABLE IF EXISTS `student`;

CREATE TABLE `student` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` char(10) NOT NULL,
  `last_name` char(10) NOT NULL,
  `password` char(10) NOT NULL,
  `Type` enum('S','T') DEFAULT NULL,
  `usernameEEEE` char(10) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `student` */

insert  into `student`(`ID`,`first_name`,`last_name`,`password`,`Type`,`usernameEEEE`) values (2,'Student2','SL2','pass','T','student2'),(3,'Student3','SL3','Pass','T','student3'),(4,'zxcv','zxcv','zxcv','S','zxcv'),(7,'xczvbcxvb','234','nbbvn','T','xcv');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
